/// <reference types="cypress" />

context('Assertions', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/')
  })

  describe('SignUp Pages step 1', () => {
    it('should have page title', () => {
  
        cy.get('[data-testid="signup-title"]',{timeout: 20000})
        .should('have.length', 1);
    
        // checking the text of the element 
        cy.get('[data-testid="signup-title"]',{timeout: 20000})
        .should('have.text', "Let's set up your account");
        
    })
  })
})
