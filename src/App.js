import React, { Fragment } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./styles/MaterialUI-Theme/theme";
import NoSsr from "@material-ui/core/NoSsr";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import SignUpPage from "./pages/SignUpPage/SignUpPage";

function App() {
  
  return (
    <Fragment>
      <NoSsr>
        <MuiThemeProvider theme={theme}>
          <div>
            <Router>
              <div className="App">
                <Switch>
                  <Route exact path="/" component={SignUpPage} />
                </Switch>
              </div>
            </Router>
          </div>
        </MuiThemeProvider>
      </NoSsr>
    </Fragment>
  );
}

export default App;
