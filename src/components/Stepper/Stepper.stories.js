import React from 'react';
import Stepper from './Stepper';

 const examplesStepper = {
  title: 'Stepper',
  component: Stepper,
};

export default examplesStepper;

const Template= (args) => <Stepper {...args} />;

export const Active1 = Template.bind({});
Active1.args = {
  activeStep: 1,
  steps: 5,
};

export const Active2 = Template.bind({});
Active2.args = {
  activeStep: 2,
  steps: 6,
};

