import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import Typography from "@material-ui/core/Typography";
import styles from "./Stepper.module.scss";
import PropTypes from "prop-types";

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    flexGrow: 1,
    background: "transparent",
    backgroundColor: "transparent",
  },
  typography: {
    fontWeight: 600,
  },
});

const Stepper = ({ activeStep = 0, steps }) => {
  const classes = useStyles();

  return (
    <div className={styles.container}>
      <Typography
        classes={{ root: classes.typography }}
      >{`Step ${activeStep} of ${steps}`}</Typography>
      <MobileStepper
        variant="dots"
        steps={steps}
        position="static"
        activeStep={activeStep === 0 ? 0 : (activeStep - 1)}
        className={classes.root}
      />
    </div>
  );
};

export default Stepper;

Stepper.propTypes = {
  activeStep: PropTypes.number.isRequired,
  steps: PropTypes.number.isRequired,
};
