import React, { Fragment } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";
import { useField } from "formik";
import PropTypes from "prop-types";

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    "& > span": {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

const Dropdown = ({ setFieldValue, name, options, id, label, ...props }) => {
  const classes = useStyles();
  const [field, meta, helpers] = useField({name, ...props});
  const { error } = meta;
  const {setValue} = helpers;
  return (
    <Fragment>
      <Autocomplete
        id={id}
        style={{ width: "100%" }}
        options={options}
        classes={{
          option: classes.option,
        }}
        autoHighlight
        getOptionLabel={(option) => option.label}
        renderOption={(option) => (
          <Fragment>
            <span>{option.label} </span>
          </Fragment>
        )}
        onChange={(e, value) => {
          if (value === null || value === undefined) {
            setValue("");
          } else {
            setValue(value.value);
          }
        }}
        renderInput={(params) => (
          <Fragment>
            <TextField
              {...params}
              {...props}
              {...field}
              label={label}
              error={error ? true : false}
              fullWidth
            />
          </Fragment>
        )}
      />

      {error ? <div className="text-danger mb-2">{error}</div> : null}
    </Fragment>
  );
};

export default Dropdown;

Dropdown.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
};
