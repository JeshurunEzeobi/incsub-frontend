import React from "react";
import styles from "./button.module.scss";
import clsx from "clsx";

export const Button = ({
  color = "primary",
  size = "medium",
  backgroundColor,
  label,
  ...props
}) => {
  const mode =
    color === "primary" ? styles.buttonPrimary : styles.buttonSecondary;
  return (
    <button
      type="button"
      className={clsx(
        styles.brandButton,
        size === "medium" && styles.buttonMedium,
        size === "small" && styles.buttonSmall,
        size === "large" && styles.buttonLarge,
        mode
      )}
      style={{ backgroundColor }}
      {...props}
    >
      {label}
    </button>
  );
};

export default Button;
