import React from 'react';
import Button from "./Button";
import { mount, shallow } from "enzyme";
import { render, fireEvent, waitFor } from "@testing-library/react";


describe("Button Component tests", () => {
    it("should render button without crashing", async ()=> {
         shallow(<Button label="Great year"/>);
    });

    it("small button", async () => {
        const { getByText} = render(<Button label="Awesome"/>);

        const button = await getByText("Awesome");
        expect(button).toBeInTheDocument();
    });

    it("should handle click events", async ()=> {
        const handleClick = jest.fn();
        const { getByText} = render(<Button onClick={handleClick} label="Awesome"/>);
        const button = getByText("Awesome");

        await fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });

    it("show small button", async ()=> {
        const { getByText } = render(
            <Button size="small" label="Awesome"/>
        );

       const smallButton = await getByText("Awesome");
       expect(smallButton).toBeInTheDocument();

    })

})