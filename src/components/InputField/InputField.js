import React, {Fragment} from "react";
import { useField } from "formik";
import TextField from "@material-ui/core/TextField";

const InputField = ({ ...props }) => {
  const [field, meta] = useField(props);
  return(
    <Fragment>
    <TextField
      {...field}
      {...props}
      error={meta.touched && meta.error ? true : false}
      fullWidth
    />
    {meta.touched && meta.error ? (
      <div className="text-danger mb-3">{meta.error}</div>
    ) : null}
  </Fragment>
  );
};

export default InputField;
