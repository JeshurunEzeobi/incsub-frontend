import React from "react";
import Grid from "@material-ui/core/Grid";
import SignUpForm from "./components/SignUpForm/SignUpForm";
import Stepper from "../../components/Stepper/Stepper";
import styles from "./SignUpLayout.module.scss";
import SignUpBanner from "../SignUpLayout/components/SignUpBanner/SignUpBanner";
import clsx from "clsx";

const SignUpLayout = ({ onComplete }) => {
  return (
    <Grid classes={{ root: styles.root }} container>
      <Grid item xs={12} sm={12} md={7}>
        <div className={clsx(styles.stepperSection, "mt-4")}>
          <Stepper activeStep={1} steps={3} />
        </div>
        <SignUpForm onComplete={onComplete} />
      </Grid>
      <Grid item xs={12} sm={12} md={5}>
        <SignUpBanner />
      </Grid>
    </Grid>
  );
};

export default SignUpLayout;
