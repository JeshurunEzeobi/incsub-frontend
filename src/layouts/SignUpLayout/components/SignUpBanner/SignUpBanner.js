import React from "react";
import styles from "./SignUpBanner.module.scss";

const SignUpBanner = () => {
  return (
    <div className={styles.container}>
      <h2 className={styles.title}>Dummy Heading</h2>
      <p className={styles.body}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua incus
      </p>
    </div>
  );
};

export default SignUpBanner;
