import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import styles from "./SignUpForm.module.scss";
import InputField from "../../../../components/InputField/InputField";
import Dropdown from "../../../../components/Dropdown/Dropdown";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { isError, isTouched } from "./checkErrorsHelper";
import Typography from "@material-ui/core/Typography";

const SignUpForm = ({ onComplete }) => {
  const [values, setValues] = useState({
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const categoryOptions = [
    { value: "Admin", label: "Admin" },
    { value: "Student", label: "Student" },
    { value: "Teacher", label: "Teacher" },
  ];

  return (
    <Grid container>
      <Grid item xs={12} sm={12} md={3}></Grid>
      <Grid item xs={12} sm={12} md={6}>
        <div className={styles.container}>
          <Formik
            initialValues={{ name: "", email: "", password: "", category: "" }}
            validationSchema={Yup.object({
              password: Yup.string()
                .required("This field is required")
                .min(8, "Password must be at least 8 characters")
                .max(200, "Password should not exceed 200 characters"),
              email: Yup.string()
                .email("Invalid email address")
                .required("This field is required"),
              category: Yup.string().required("This field is required"),
              name: Yup.string().required("This field is required"),
            })}
            onSubmit={(values, { setSubmitting }) => {
              onComplete(values);
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <h2 className={styles.title} data-testid="signup-title">Let's set up your account</h2>
                <div className="mb-4">
                  <p>
                    Already have an account?
                    <span className={styles.textBlue}>
                      <a href="/signIn">Sign in</a>
                    </span>
                  </p>
                </div>
                <InputField
                  label={"Your name"}
                  name="name"
                  type="text"
                  margin="normal"
                  className="mt-0 mb-2"
                  variant="outlined"
                />
                <InputField
                  label={"Email address"}
                  name="email"
                  type="email"
                  margin="normal"
                  className="mt-0 mb-2"
                  variant="outlined"
                />
                <Dropdown
                  id="signup-category"
                  label="I would describe my user type as"
                  options={categoryOptions}
                  name="category"
                  variant="outlined"
                  className="mt-0 mb-2"
                />
                <InputField
                  label={"Password"}
                  name="password"
                  type={values.showPassword ? "text" : "password"}
                  margin="normal"
                  className="mt-0 mb-2"
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment>
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={(event) => {
                            handleMouseDownPassword(event);
                          }}
                        >
                          {values.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <Typography color="textSecondary">
                  minimum 8 characters
                </Typography>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  classes={{ root: styles.nextButton }}
                  className={"w-100 mb-4 mt-4"}
                  disabled={
                    !isError(errors) && isTouched(touched) ? false : true
                  }
                >
                  Next
                </Button>
                <Typography classes={{ root: styles.bottomText }}>
                  By clicking the "Next" button, you agree to creating a free
                  account, and to{" "}
                  <span className={styles.textBlue}>Terms of Service</span> and{" "}
                  <span className={styles.textBlue}>Privacy Policy</span>
                </Typography>
              </Form>
            )}
          </Formik>
        </div>
      </Grid>
      <Grid item xs={12} sm={12} md={3}></Grid>
    </Grid>
  );
};

export default SignUpForm;
