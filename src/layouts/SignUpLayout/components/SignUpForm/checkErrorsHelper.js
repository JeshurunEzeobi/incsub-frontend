export const isError = (errors) => {
  if (
    errors.name !== undefined ||
    errors.email !== undefined ||
    errors.password !== undefined ||
    errors.category !== undefined
  ) {
    return true;
  } else {
    return false;
  }
};

export const isTouched = (touched) => {
  if (
    touched.name !== undefined ||
    touched.email !== undefined ||
    touched.password !== undefined ||
    touched.category !== undefined
  ) {
    return true;
  } else {
    return false;
  }
};
