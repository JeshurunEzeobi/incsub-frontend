import React from 'react';
import SignUpForm from './SignUpForm';

 const SampleSignUpForm = {
  title: 'SignUpForm',
  component: SignUpForm,
};

export default SampleSignUpForm;

const Template= (args) => <SignUpForm {...args} />;

export const Form = Template.bind({});
Form.args = {
};
