import React, { useEffect } from "react";
import SignUpLayout from "../../layouts/SignUpLayout/SignUpLayout";
import styles from "./SignUpPage.module.scss";

const SignUpPage = () => {
  useEffect(() => {
    //you can do logic here
  }, []);

  const handleComplete = (values) => {
    console.log(values);
  };

  return (
    <div className={styles.container}>
      <SignUpLayout onComplete={handleComplete} />
    </div>
  );
};

export default SignUpPage;
